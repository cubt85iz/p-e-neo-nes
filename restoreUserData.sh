#!/usr/bin/env bash

USERDATA_DIR=/userdata
BACKUPS_DIR=$USERDATA_DIR/backups
ROMS_DIR=$USERDATA_DIR/roms

# Create folder to store backup files if it doesn't exist.
if [ -d $BACKUPS_DIR ]; then
  # Get latest backup file.
  BACKUP_FILE=$(ls $BACKUPS_DIR | tail -n 1)

  # Restore latest backup of $USERDATA_DIR.
  tar xva --file=$BACKUPS_DIR/$BACKUP_FILE $USERDATA_DIR
fi
