#!/usr/bin/env bash
# Batocera Userdata Backup
# ROM files, BIOS files, and High-Definition Textures are synchronized using Syncthing.
# Include the following files/directories from archive.
#	| cheats
#	| decorations
#	| extractions
#	| overlay
#	| saves
#	| screenshots
#	| scripts
#	| splash
#	| system/configs/cron
#	| system/configs/emulationstation/
#	| system/configs/emulationstation/collections
#	| system/configs/emulationstation/es_settings.cfg
#	| system/configs/syncthing
#	| themes

# Sleep 10s until time has updated.
# Raspberry Pi doesn't track time on board.
YEAR=$(date '+%Y')
while [ $YEAR -lt 2022 ]; do
  sleep 500ms
  YEAR=$(date '+%Y')
done;

# Initialize variables for backup operation
HOST=$(hostname)
USERDATA_DIR=/userdata
BACKUPS_DIR=$USERDATA_DIR/backups
ROMS_DIR=$USERDATA_DIR/roms
BACKUP_FILE=$HOST.$(date '+%Y%m%d').tar.gz

# Write variable values to screen.
echo "USERDATA_DIR: ${USERDATA_DIR}"
echo "BACKUPS_DIR: ${BACKUPS_DIR}"
echo "ROMS_DIR: ${ROMS_DIR}"
echo "BACKUP_FILE: ${BACKUP_FILE}"

# Create folder to store backup files if it doesn't exist.
test -d $BACKUPS_DIR || mkdir $BACKUPS_DIR

# Create backup if one doesn't exist.
if [ ! -f $BACKUPS_DIR/$BACKUP_FILE ]; then
  # Create backup of $USERDATA_DIR and store in $BACKUPS_DIR.
  # tar ca -X exclude.lst -T include.lst --directory=$USERDATA_DIR --file=$BACKUPS_DIR/$BACKUP_FILE

  # Backup userdata including gamelist.xml files to preserve favorites.
  find /userdata -iname gamelist.xml -print | sed 's/\/userdata\///' | tar ca -X /userdata/scripts/userdata/exclude.lst -T - -T /userdata/scripts/userdata/include.lst --directory=$USERDATA_DIR --file=$BACKUPS_DIR/$BACKUP_FILE
fi

# Remove backups older than 30 days.
find $BACKUPS_DIR -type f -mtime +30 -exec rm {} \;
