P&E Neo-NES powered by Batocera Instruction Manual
_(Version 1.0 [27 March 2022])_

# Congratulations!
You are now the proud owner of the (one and only) P&E Neo-NES powered by Batocera! The Neo-NES contains all the games released for the Nintendo Entertainment System (NES), Super Nintendo, Sega Genesis, & Nintendo 64. It includes classics like Bases Loaded, Mario Kart 64, Diddy Kong Racing, Paper Mario 64 and many more. Enjoy!



## Neo-NES Hardware Info
### Internals
The Neo-NES is powered by an overclocked Raspberry Pi 4B. If you flip open the lid of the cartridge slot you'll find a cartridge. The cartridge is actually a hard drive in a plastic case.

### Front
On the front you will find buttons for Power & Reset as well as two USB ports for connecting controllers. I tested and included an unpowered USB hub and found that it will allow you to use up to three additional controllers.

### Side
On the side of the device is a MicroSD card slot. This card slot will need to be utilized if you ever find yourself needing to recover the system.

### Bottom
On the bottom of the device is a compartment for storing MicroSD cards. Hopefully, you should never need it, but I have included a recovery card that can be used to restore the system in the event of a failure. This comparment also includes notches on the outside which can be used to remove the cartridge from it's slot. See the included documentation for the case for an image of the compartment.

### Rear
On the rear of the device are all of the I/O ports. There are ports for ethernet, power, audio out, and two HDMI ports for audio and video. The device also supports WiFi if you prefer to utilize that.

### Controllers
Included with the Neo-NES are two 8BitDo USB controllers. These controllers are currently available on Amazon and locally at Best Buy. They are very popular in the Retro Gaming Community and well-supported. There are also wireless Bluetooth versions of the controller available.



## Batocera
The Neo-NES is powered by an open-source Linux project named Batocera. Batocera provides operating system images for the Raspberry Pi 4 and updates can be found at their website (https://www.batocera.org). Batocera includes all the required emulators for the systems I noted above.

### Navigation
Navigation in Batocera is fairly straightforward. However, the navigation options availabe change depending on the selected view. This section will attempt to clear up any confusion that may occur when navigating through the Batocera user-interface.

#### Systems View
When you power on the Neo-NES you will be presented with the Systems View which will list all of the systems with games available. This view will also include categories for 2-player games, 4-player games, Favorites, and more. At the bottom of the Systems View is navigation information.

##### Quick Access Menu
Pressing the Select button while on the Systems View will bring up the Quick Access Menu. This menu includes options for viewing the Batocera Guide (recommended), restarting the Neo-NES & powering off the Neo-NES.

##### Main Menu
Pressing the Start button while on the Systems View will bring up the Main Menu. This menu contains most of the configuration options for the Neo-NES, including things like Networking and Controller configuration. I'd suggest spending some time navigating this menu to review the options available.

#### Games List View
After selecting a system, the Games List View will be displayed. The Games List View includes all of the games installed for the selected system. The Navigation information for this view may include two actions associated with a button. For example, if you press the X button, the Save States Manager will be displayed. However, if you long-press the X button, the current game will be added to your favorites list.

##### View Options Menu
Pressing the Select button while on the Games List View will bring up the View Options Menu. This menu includes options for changing the display format for the games and advanced system options which can allow you to select a different emulator for the selected system.

##### Main Menu
Pressing the Start button while on the Games List View also brings up the Main Menu. However, some of the commands may only work for the selected system. For example, the Scraper will only scrape for content for the selected System.

##### Game Panel
Long-pressing the button to select a game will bring up the Game Panel on the right-side of the screen. This panel is **VERY** useful. It allows you to view available media for the game like the map or the game manual. You can also view and manage the save states for a game, open the advanced game options, or directly edit the metadata for the game. 

#### In-Game Hotkeys
There are a number of in-game hotkeys that can be utilized to perform actions like saving, loading and exiting the game. Here's a quick cheatsheet that's specific to the controllers provided with the Neo-NES.

SELECT + B: View Quick Menu for Emulator / Pause
SELECT + Y: Perform a quick save of the game to an emulator save slot
SELECT + X: Perform a quick load of the game from an emulator save slot
SELECT + A: Restart the game
SELECT + Start: Exit the game

There are more hotkeys available. I suggest reviewing the Batocera Guide for more information. See the Quick Access Menu section above.

### UI Mode
Batocera provides a parental control mode called UI Mode which allows parents to configure the system to display only games they have approved for play. The UI Mode setting is located in the Main Menu. Select the System Settings menu item and select UI Mode to change it. There are three options Full, Kid, & Kiosk (irrelevant). Changing the UI Mode to full will display all games and allow parents to designate games as kid-safe.

NOTE: If you change the configured theme for the system, it could display video content or imagery for games that isn't kid-safe. I've preselected a theme that does not display this content.

#### Designating Games as Kid-Safe
Marking games as kid-safe isn't difficult, but can be a little laborious. Before you begin, you must first enable Full UI Mode to display all the games. Navigate to the game you wish to enable and open the Game Panel. Select the "Edit this Game's Metadata" menu item. Navigate to the bottom of the window and toggle the Kidgame option. Save your changes and enable the Kid UI Mode to test.

### Networking
Networking can be configured from the Main Menu. Select Network Settings and provide the necessary information to connect to your WiFi network. Ethernet connections should just work.


## Disaster Recovery & Backups
Accidents happen. This section describes how backups are performed and maintained as well as the procedures for performing disaster recovery in the event of a failure.

### Backups
I've included a script named backupUserData.sh which includes code to perform a backup of the system and stores it on the system in a compressed archive in the /userdata/backups folder. Each backup contains the save states for each of the games as well as the system configuration. The items stored in the /userdata/backups folder are backed up to my home server using an open-source file synchronization application named Syncthing. Syncthing is also used to synchronize game files and script updates. Full details can be found by visiting "http://<ip_address_of_neo-nes>:8384" in your browser. Credentials are included with the documentation delivered with the system.

#### Frequency
Backups are performed when the system is powered on if a backup has not already been taken for that day. All backups older than 30 days are deleted.

#### Contents
Review backupUserData.sh, include.lst & exclude.lst to determine the files included in backup files.

### Disaster Recovery
The steps have been noted and a rescue MicroSD card has been included with the system. A future revision of this document will include the precise steps that need to be performed to recover the system.

## Updates
Scan this QR code for code and documentation updates.

![QR Code for Updates](./qrcode_for_updates.png "Neo-NES Repository")